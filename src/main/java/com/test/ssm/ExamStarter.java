package com.test.ssm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

/**
 * @author 徒有琴
 */
@SpringBootApplication
@MapperScan(basePackages = "com.test.ssm.dao")
public class ExamStarter {
    public static void main(String[] args) {
        SpringApplication.run(ExamStarter.class, args);
    }
}
