package com.test.ssm.service;

import com.test.ssm.pojo.AccountType;

import java.util.List;

public interface TypeService {
    List<AccountType> getAllType();
}
