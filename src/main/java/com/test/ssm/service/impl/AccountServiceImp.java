package com.test.ssm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.test.ssm.dao.AccountDAO;
import com.test.ssm.pojo.AccountSearch;
import com.test.ssm.pojo.PublicAccount;
import com.test.ssm.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 徒有琴
 */
@Service
public class AccountServiceImp implements AccountService {
    @Autowired
    private AccountDAO accountDAO;

    @Override
    public PageInfo<PublicAccount> getAccount(AccountSearch search, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(accountDAO.getAccount(search), 5);
    }

    @Override
    public void updateAccount(PublicAccount account) {
        accountDAO.updateAccount(account);
    }

    @Override
    public PublicAccount getAccountById(Integer id) {
        return accountDAO.getAccountById(id);
    }

    @Override
    public void addAccount(PublicAccount account) {
        accountDAO.addAccount(account);
    }

    @Override
    public void deleteAccount(Integer id) {
        accountDAO.deleteAccount(id);
    }
}
