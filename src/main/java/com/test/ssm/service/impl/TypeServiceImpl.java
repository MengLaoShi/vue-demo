package com.test.ssm.service.impl;

import com.test.ssm.dao.TypeDAO;
import com.test.ssm.pojo.AccountType;
import com.test.ssm.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 徒有琴
 */
@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private TypeDAO typeDAO;

    @Override
    public List<AccountType> getAllType() {
        return typeDAO.getAllType();
    }
}
