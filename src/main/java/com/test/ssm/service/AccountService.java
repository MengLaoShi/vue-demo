package com.test.ssm.service;

import com.github.pagehelper.PageInfo;
import com.test.ssm.pojo.AccountSearch;
import com.test.ssm.pojo.PublicAccount;

public interface AccountService {
    PageInfo<PublicAccount> getAccount(AccountSearch search, Integer pageNum, Integer pageSize);

    void updateAccount(PublicAccount account);

    void addAccount(PublicAccount account);

    void deleteAccount(Integer id);

    PublicAccount getAccountById(Integer id);
}
