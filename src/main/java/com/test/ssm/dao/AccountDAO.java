package com.test.ssm.dao;

import com.test.ssm.pojo.AccountSearch;
import com.test.ssm.pojo.PublicAccount;

import java.util.List;

public interface AccountDAO {
    List<PublicAccount> getAccount(AccountSearch search);

    void updateAccount(PublicAccount account);

    PublicAccount getAccountById(Integer id);

    void addAccount(PublicAccount account);

    void deleteAccount(Integer id);
}
