package com.test.ssm.dao;

import com.test.ssm.pojo.AccountType;

import java.util.List;

public interface TypeDAO {
    List<AccountType> getAllType();
}
