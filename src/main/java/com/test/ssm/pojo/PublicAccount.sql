-- auto Generated on 2020-01-07 10:25:27 
-- DROP TABLE IF EXISTS `public_account`; 
CREATE TABLE `public_account`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `account` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'account',
    `name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'name',
    `type_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'typeId',
    `created_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'createdTime',
    `status` INT (11) NOT NULL DEFAULT -1 COMMENT 'status',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`public_account`';
