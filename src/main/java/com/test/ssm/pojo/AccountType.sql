-- auto Generated on 2020-01-07 10:23:00 
-- DROP TABLE IF EXISTS `account_type`; 
CREATE TABLE `account_type`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'name',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`account_type`';
-- auto Generated on 2020-01-07 10:23:00
-- DROP TABLE IF EXISTS `account_type`;
CREATE TABLE `account_type`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'name',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`account_type`';
INSERT  into account_type(name) VALUES ('教育');
INSERT  into account_type(name) VALUES ('政府');
INSERT  into account_type(name) VALUES ('娱乐');

CREATE TABLE `public_account`(
    `id` INT (11) NOT NULL AUTO_INCREMENT COMMENT 'id',
    `account` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'account',
    `name` VARCHAR (50) NOT NULL DEFAULT '' COMMENT 'name',
    `type_id` INT (11) NOT NULL DEFAULT -1 COMMENT 'typeId',
    `created_time` DATETIME NOT NULL DEFAULT '1000-01-01 00:00:00' COMMENT 'createdTime',
    `status` INT (11) NOT NULL DEFAULT -1 COMMENT 'status',
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT '`public_account`';

INSERT into public_account(account, name, type_id, created_time, status) VALUES ('bb2b2bb','奔波儿灞',1,now(),1);
INSERT into public_account(account, name, type_id, created_time, status) VALUES ('sadasd','千锋',2,now(),1);
INSERT into public_account(account, name, type_id, created_time, status) VALUES ('bbbbb','暴走漫画',1,now(),1);
INSERT into public_account(account, name, type_id, created_time, status) VALUES ('aaaas','bilibili',1,now(),2);