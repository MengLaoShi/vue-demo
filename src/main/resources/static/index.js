var vm = new Vue({
    el: "#index",
    data: {
        typeList: {},
        accountList: {},
        accountSearch: {}
    },
    methods: {
        getTypes: function (event) {
            $.ajax({
                url: "/types",
                success: function (json) {
                    vm.typeList = json;
                }
            })
        },
        getList: function (pn) {
            $.ajax({
                url: "/search?pageNum=" + pn,
                data: this.accountSearch,
                success: function (json) {
                    vm.accountList = json;
                }
            })
        }
    }
    , created: function () {//在控件初始化完成之后加载
        this.getTypes();//加载分类列表
        this.getList(1);//加载数据列表
    }
})