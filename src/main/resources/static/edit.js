//自定义组件
var vm = new Vue({
    el: "#update",
    data: {
        typeList: {},
        publicAccount: {type:{id:""}}
    },
    methods: {
        getTypes: function (event) {
            $.ajax({
                url: "types",
                success: function (json) {
                    vm.typeList = json
                }
            })
        }, getUrlKey: function (name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [, ""])[1].replace(/\+/g, '%20')) || null
        }, info: function (id) {
            if(id==null||id==""){
                return;
            }
            $.ajax({
                url: "info?id=" + id,
                success: function (json) {
                    vm.publicAccount = json
                }
            })
        }
    }
    , created: function () {//
        this.getTypes();
        var id = this.getUrlKey("id");
        this.info(id);
    }
})

